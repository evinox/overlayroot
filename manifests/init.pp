# Overlayroot setup
class overlayroot {

  #contain overlayroot::reboot

  ensure_packages(['overlayroot'], { ensure => 'present' })

  file { '/etc/overlayroot.conf':
    ensure  => present,
    source  => 'puppet:///modules/overlayroot/overlayroot.conf',
    notify  => Exec['remove-apparmor'],
    require => Package['overlayroot'],
  }

  file { '/opt/puppetlabs/facter/facts.d/overlayenabled.sh':
    ensure  => present,
    source  => 'puppet:///modules/overlayroot/overlayenabled.sh',
    group   => 'root',
    owner   => 'root',
    mode    => '0755',
    require => Package['overlayroot'],
  }

  file { '/opt/puppetlabs/facter/facts.d/partition.sh':
    ensure  => present,
    source  => 'puppet:///modules/overlayroot/partition.sh',
    group   => 'root',
    owner   => 'root',
    mode    => '0755',
    require => Package['overlayroot'],
  }

  file { '/sbin/grubby':
    ensure  => present,
    source  => 'puppet:///modules/overlayroot/grubby',
    group   => 'root',
    owner   => 'root',
    mode    => '0755',
    require => Package['overlayroot'],
  }

  # Todo: check if this is necessary otherwise remove, when introduced 
  # Apparmor stops ntpd to run
  exec {'remove-apparmor':
    cwd         => '/home/evxadmin',
    command     => '/bin/systemctl stop apparmor  && /bin/systemctl disable apparmor', #'/usr/sbin/update-rc.d apparmor remove',
    creates     => '/tmp/apparmor.txt',
    refreshonly => true,
    #notify  => File['/etc/grub.d/40_custom']
  }


  # static way to update grub menu
  # doesn't work if kernel version is different from the
  # the one in the file

  # file { '/etc/grub.d/40_custom':
  #   ensure => present,
  #   source  => 'puppet:///modules/overlayroot/40_custom',
  #   group   => 'root',
  #   owner   => 'root',
  #   mode    => '755',
  #   notify  => Exec['update-grub']
  # }
  #
  # exec {'update-grub':
  #   cwd     => '/home/evxadmin',
  #   command => '/usr/sbin/update-grub',
  #   refreshonly => true,
  # }


  grub_config { 'GRUB_TIMEOUT':
    value => '1'
  }

  #kernel_parameter { 'all':
  #  ensure => present,
  #  value  => 'cgroup_enable=memory swapaccount=1'
  #}
  kernel_parameter { 'swapaccount':
    bootmode => 'all',
    value    => '1'
  }

  kernel_parameter { 'cgroup_enable':
    bootmode => 'all',
    value    => 'memory'
  }

  # using a modified version of the herculesteam/augeasproviders_grub
  # in order to make this work I created a copy of the repository in bitbucket and
  # edited
  # - grub2-set-defult to grub-set-default in file grub2.rb
  # - output file name from 05_puppet-managed to 40_custom
  # - format of the file adding 3 comments to it.
  # - also this requires grubby to aplly the changes. Grubby is not part of Ubuntu
  # anymore hence it needs to be compiled in the node Grubby requires libraries:
  # - linux-vdso.so.1 =>  (0x00007ffd74ffa000)
  # - libblkid.so.1 => /lib/x86_64-linux-gnu/libblkid.so.1 (0x00007f5491013000)
  # - libpopt.so.0 => /lib/x86_64-linux-gnu/libpopt.so.0 (0x00007f5490e07000)
  # - libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f5490a3d000)
  # - libuuid.so.1 => /lib/x86_64-linux-gnu/libuuid.so.1 (0x00007f5490838000)
  # - /lib64/ld-linux-x86-64.so.2 (0x00007f5491254000)

  # https://github.com/rhboot/grubby
  # msdos1 or gpt1
  notice("Partition ${facts['partition']}")
  case $facts['partition'] {
    'msdos': {
      $part = 'msdos1'
      notice("this is an msdos partition ${part}")
    }
    'gpt': {
      $part = 'gpt1'
      notice("this is an gpt partition ${part}")
    }
    default: {
      $part = 'gpt1'
      notice("this is an default partition ${part}")
    }
  }
  grub_menuentry { 'Ubuntu, overlayroot disabled':
    add_defaults_on_creation => true,
    load_video               => true,
    root                     => "(hd0,${part})",
    kernel                   => ':preserve:',
    initrd                   => ':preserve:',
    kernel_options           => [':preserve:', 'overlayroot=disabled'],
  }

}
