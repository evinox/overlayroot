# overayroot::reboot

define overlayroot::reboot(
  Boolean $set_overlay_enabled,
)
  {

    if $set_overlay_enabled {
      if $facts['overlayroot'] == 'enabled' {

        notify { 'overlayroot already enabled, do nothing':
          loglevel => 'info'
        }

      } elsif $facts['overlayroot'] == 'disabled' {

        notify { 'Enabling overlayroot':
          loglevel => 'info'
        }

        reboot { 'rebootsystem':
          message   => 'reboot for enabling overlayroot',
          apply     => 'immediately',
          subscribe => Notify['Enabling overlayroot']
        }

      } else {
        notify { 'Overlayroot not found':
          loglevel => 'warning'
        }
      }

    } else { #set overlayenabled = false

      if $facts['overlayroot'] == 'enabled' {
        notify { 'Overlayroot enabled rebooting device':
          loglevel => 'info'
        }

        exec { 'remove_ro':
          command => '/bin/mount -o remount,rw /dev/sda1 /boot',
          before  => Exec['grubreboot']
        }

        exec { 'grubreboot':
          command => '/usr/sbin/grub-reboot 2',
          before  => Reboot['disable-overlary']
        }

        reboot { 'disable-overlary':
          message   => 'reboot for disabling overlayroot',
          apply     => 'immediately',
          subscribe => Exec['grubreboot']
        }

      } elsif $facts['overlayroot'] == 'disabled' {
        notify { 'overlayroot disabled do nothing':
          loglevel => 'info'
        }
      }  else {
        notify { 'Overlayroot not found1':
          loglevel => 'warning'
        }
      }
    }


  }

