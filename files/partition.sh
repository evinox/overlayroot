#!/bin/bash
DEV=/dev/sda
part=$( parted -s $DEV print | grep  -i '^Partition Table' )
#echo $part
if [[ $part == *"gpt"* ]]; then
  echo "partition=gpt"
elif [[ $part == *"msdos"* ]]; then
  echo "partition=msdos"
fi
